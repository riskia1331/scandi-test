<?php include 'template/header.php'; ?>
    <form action="process?action=add" method="post" id="product_form">
    <header>
        <div class="row">
            <div class="col-8"><h1>Product Add</h1></div>
            <div class="col-4">
                <button type="button" class="btn btn-danger" onclick="cancelOnClick('index')">Cancel</button>
                <button type="submit" class="btn btn-secondary">Save</button>
            </div>
        </div>
    </header>
    <main class="main-content">
        <div class="mb-3 row">
            <label for="inputPassword" class="col-sm-2 col-form-label">SKU</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="sku" name="sku">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Price ($)</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="price" name="price">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Type Switcher</label>
            <div class="col-sm-10">
                <select class="form-select" id="productType" name="productType">
                    <option selected>Type Switcher</option>
                    <option value="1">DVD</option>
                    <option value="2">Furniture</option>
                    <option value="3">Book</option>
                </select>
            </div>
        </div>
        <div id='load'></div>
        
        
    </main>
    </form>    

    <footer>
        <p>Scandiweb Test Assignment - Riskia (RDR)</p>
    </footer>
</body>
    <script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</html>