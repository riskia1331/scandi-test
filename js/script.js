    window.onload = function() {
        var type = document.getElementById('productType');
        type.addEventListener('change', loadForm);

        var home = document.getElementById('addOnClick');
        home.addEventListener('click', loadForm);
    }
    
    function loadForm() {
        var text = "";
        var itemSelected;

        itemSelected = document.getElementById("productType").value;
        switch (itemSelected) {
            case "1":
                text = "<div class='mb-3 row'>"+
                    "<label class='col-sm-2 col-form-label'>Size (MB)</label>"+
                    "<div class='col-sm-10'>"+
                        "<input type='text' class='form-control' id='size' name='size'>"+
                    "</div>"+
                "</div>";
                break;
            case "2":
                text = "<div class='mb-3 row'>"+
                    "<label class='col-sm-2 col-form-label'>Height (CM)</label>"+
                    "<div class='col-sm-10'>"+
                        "<input type='text' class='form-control' id='height' name='height'>"+
                    "</div>"+
                "</div>"+
                "<div class='mb-3 row'>"+
                    "<label class='col-sm-2 col-form-label'>Width (CM)</label>"+
                    "<div class='col-sm-10'>"+
                        "<input type='text' class='form-control' id='width' name='width'>"+
                    "</div>"+
                "</div>"+
                "<div class='mb-3 row'>"+
                    "<label class='col-sm-2 col-form-label'>Length (CM)</label>"+
                    "<div class='col-sm-10'>"+
                        "<input type='text' class='form-control' id='length' name='length'>"+
                    "</div>"+
                "</div>";
                break;
            case "3":
                text = "<div class='mb-3 row'>"+
                    "<label class='col-sm-2 col-form-label'>Weight (KG)</label>"+
                    "<div class='col-sm-10'>"+
                        "<input type='text' class='form-control' id='weight' name='weight'>"+
                    "</div>"+
                "</div>";
                break;
            default:
                break;
        }
        document.getElementById("load").innerHTML = text;
    }

    function cancelOnClick(url) {
        var conf = window.confirm("Are you sure leave this page?");
        if (conf) {
            window.location = url;
        }
    }

    function addOnClick(url) {
            window.location = url;
    }

