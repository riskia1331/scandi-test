<?php 
    class database
    {
        var $host = "localhost";
        var $uname = "root";
        var $pass = "";
        var $db = "scandiweb_test";

        public function __construct()
        {
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            $this->mysqli = mysqli_connect($this->host, $this->uname, $this->pass,$this->db);
            // mysqli->set_charset('utf8mb4');
            // printf("Success... %s\n", $mysqli->host_info);
        }

        public function loadData()
        {
            $data = mysqli_query($this->mysqli, "
                SELECT tb.id, tb.sku, tb.name, tb.price, tb.id_category, tb.a, tb.b, tb.c 
                FROM (
                    SELECT i.id, i.sku, i.name, i.price, i.id_category, s.item_size as a, 0 as b, 0 as c FROM tb_items as i
                    JOIN tb_item_size as s on i.id = s.id_item
                    UNION ALL
                    SELECT i.id, i.sku, i.name, i.price, i.id_category, w.weight as a, 0 as b, 0 as c FROM tb_items as i 
                    JOIN tb_item_weght as w on i.id = w.id_item
                    UNION ALL
                    SELECT i.id, i.sku, i.name, i.price, i.id_category, d.height, d.width, d.length FROM tb_items as i 
                    JOIN tb_item_dimension as d on i.id = d.id_item ) as tb
                ORDER BY id
            ");
            
            while($row = mysqli_fetch_array($data)){
                $result[] = $row;
            }
            return $result;
            
        }

        public function deleteData($id)
        {
            $data = mysqli_query($this->mysqli, "DELETE FROM tb_items WHERE id=$id");
        }

        public function addDataItem($sku, $name, $price, $id_category, $a, $b, $c)
        {
            mysqli_query($this->mysqli, "INSERT INTO tb_items VALUES ('', '$sku', '$name', '$price', '$id_category')");
            $getId = $this->getIdData($sku);
            $id = $getId['id'];
            // print_r($id);
            $this->addDataDetail($id_category, $id,$a, $b, $c);
        }

        public function getIdData($sku)
        {
            $data = mysqli_query($this->mysqli, "
               
                SELECT id FROM tb_items
                WHERE sku = '$sku'
                    
            ");
            return $data->fetch_assoc();
        }

        public function addDataDetail($id_category, $id, $a, $b, $c)
        {
            switch ($id_category) {
                case '1':
                    mysqli_query($this->mysqli, "INSERT INTO tb_item_size VALUES ('', '$id', '$a')");
                    break;
                case '2':
                    mysqli_query($this->mysqli, "INSERT INTO tb_item_dimension VALUES ('', '$id', '$a', '$b', '$c')");
                    break;
                case '3':
                    mysqli_query($this->mysqli, "INSERT INTO tb_item_weght VALUES ('', '$id', '$a')");
                    break;
                
            }
        }
    }
    
?>