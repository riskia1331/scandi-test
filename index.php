<?php 
    include 'template/header.php'; 
    include 'database.php';
    $db = new database();
?>
<form method="post" action="process?action=mass-delete">
    <header>
        <div class="row">
            <div class="col-8"><h1>Product List</h1></div>
            <div class="col-4">
                <button type="submit" class="btn btn-danger">MASS DELETE</button>
                <a href="add-product" type="button" class="btn btn-secondary">ADD</a>
            </div>
        </div>
    </header>
    <main class="main-content">
        <div class="row row-cols-4">
        <?php  foreach($db->loadData() as $dt){?>
            <div class="col col-item">
                <input class="form-check-input delete-checkbox" type="checkbox" name="checked[]" value="<?php echo $dt['id']; ?>" id="delete-checkbox">
                <div class="items-detail">
                    <?php echo strtoupper($dt['sku']); ?> <br>
                    <?php echo $dt['name']; ?> <br>
                    <?php echo $dt['price']; ?> $<br>
                    <?php 
                        switch ($dt['id_category']) {
                            case '1':
                                echo "Size: ".$dt['a']." MB";
                                break;
                            case '2':
                                echo "Dimension: ".$dt['a']."x".$dt['b']."x".$dt['c'];
                                break;
                            case '3':
                                echo "Weight: ".$dt['a']." KG";
                                break;
                        }
                    ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </main>
</form>
    <footer>
        <p>Scandiweb Test Assignment - Riskia (RDR)</p>
    </footer>
</body>
    <script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
</html>